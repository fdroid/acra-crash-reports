#!/usr/bin/env python3

import bcrypt
import copy
import gitlab
import hashlib
import json
import mailbox
import os
import re
import sys
import time
import traceback
import warnings
import yaml

from colorama import Fore, Style
from datetime import timezone
from distutils.version import LooseVersion
from email.header import decode_header, make_header
from email.utils import parseaddr, parsedate_to_datetime
from pathlib import Path

try:
    from yaml import CSafeLoader as SafeLoader
except ImportError:
    from yaml import SafeLoader


# The stack trace hash standardized to 7 lowercase chars to serve as an ID
STACK_TRACE_HASH_ID_PATTERN = re.compile(r'[0-9a-f]{7}')

# https://en.wikipedia.org/wiki/Bcrypt#Description
MESSAGE_ID_PATTERN = re.compile(r'\$2b\$12\$[A-Za-z0-9/.]{53}')

# Strip the email Signature Block https://gitlab.com/fdroid/admin/-/issues/414
SIGNATURE_BLOCK_PAT = re.compile(r'--+ \n')

STRIP_EMAIL_ADDRESSES_PATTERN = re.compile(r'\S+@\S+\.\S+')

TITLE_FROM_STACK_TRACE_PATTERN = re.compile(r'^(.*?)(?:\s+at\s+|\s+[A-Z0-9_]+=)')


class StackTraceDumper:

    basedir = Path(os.path.dirname(__file__), 'stacktraces')
    sha256dir = basedir / 'sha256'
    message_ids = set()

    def __init__(self):
        self.basedir.mkdir(exist_ok=True)
        self.sha256dir.mkdir(exist_ok=True)

    def get_brand_from_report(self, report):
        brand = report.get('BRAND')
        if not brand:
            return 'Unknown'
        bl = brand.lower()
        if bl in ('lge', 'zte', 'ngm', 'rca', 'kddi', 'semc', 'tcl', 'htc'):
            return bl.upper()
        if bl == 'htc_europe':
            return 'HTC_Europe'
        if bl == 'oneplus':
            return 'OnePlus'

        return brand.lower().title()

    def strip_block(self, text):
        if not text or text == 'null':
            return ''
        text = text.replace('<br>', '\n').replace('\r', ' ')
        text = STRIP_EMAIL_ADDRESSES_PATTERN.sub(' ', text).strip()
        if not text or text == 'null':
            return ''
        return text

    def get_message_id(self, message):
        """Get a anonymity-preserving message ID

        This is meant to be a one way Message ID, so that someone can find
        the right message if they have the email mbox.  This can be used
        to find the original email if you have the entire text of it.

        All emails should have a Message-ID, but sometimes they don't.  So
        the failsafe is to hash the whole email.

        This uses the first 22 chars of the SHA-256 of the SHA-256 of
        the Message-ID to provide a stable salt for a given message,
        given only data in the message itself. The description of the
        salt prefix is from bcrypt.gensalt()

        """

        message_id = message.get('Message-ID')
        if message_id:
            to_sha256 = str(make_header(decode_header(message_id))).encode()
        else:
            to_sha256 = message.as_bytes()
        to_bcrypt = hashlib.sha256(to_sha256).hexdigest().encode()
        salt = b'$2b$12$%s' % hashlib.sha256(to_bcrypt).hexdigest().encode()[:22]
        return bcrypt.hashpw(to_bcrypt, salt).decode()

    def generate_note(self, report, include_stack_trace=False):
        note_yaml = copy.deepcopy(report)
        stack_trace = report.get('STACK_TRACE', '')
        # sanitize JSON report to remove PII data
        accepted = (
            'ANDROID_VERSION',
            'APP_VERSION_NAME',
            'AVAILABLE_MEM_SIZE',
            'BRAND',
            'DATE',
            'Message-ID',
            'PACKAGE_NAME',
            'PHONE_MODEL',
            'STACK_TRACE_HASH',
            'USER_COMMENT',
        )
        for k in list(note_yaml.keys()):
            if k not in accepted:
                del note_yaml[k]
        description = yaml.safe_dump(
            note_yaml, allow_unicode=True, default_flow_style=False
        )
        if include_stack_trace and stack_trace:
            # manually dump for cleaner format
            description += '\n```\n## STACK_TRACE:\n\n```java\n'
            description += stack_trace.replace('\nat ', '\n  at ')
        return '```yaml\n%s\n```\n\n%s\n' % (
            description,
            '@' + ' @'.join(note_yaml.get('ping', [])),
        )

    def hardlink(self, type, data, sha256json):
        addressdir = self.basedir / type / data
        addressdir.mkdir(exist_ok=True, parents=True)
        hardlink = addressdir / sha256json.name
        if not hardlink.exists():
            hardlink.hardlink_to(sha256json)

    def process_message(self, message):
        report = {}
        body = ''
        try:
            found_attachment = False
            if message.is_multipart():
                for part in message.walk():
                    f = part.get_filename()
                    if part.get_content_type() in ('message/rfc822', 'text/plain'):
                        payload = part.get_payload(decode=True)
                        if payload:
                            body += payload.decode(errors='replace')
                    elif f and f.startswith('ACRA-report.stacktrace'):
                        payload = part.get_payload(decode=True)
                        sha256 = hashlib.sha256(payload).hexdigest()
                        sha256json = self.sha256dir / ('%s.json' % sha256)
                        with (sha256json).open('wb') as fp:
                            fp.write(payload)
                        report = json.loads(payload)
                        found_attachment = True
            if not found_attachment:
                return

            stack_trace_hash = report['STACK_TRACE_HASH'][:7].lower()  # normalized
            self.hardlink('stackTraceHash', stack_trace_hash, sha256json)
            self.hardlink('versionName', report['APP_VERSION_NAME'], sha256json)
            self.hardlink('packageName', report['PACKAGE_NAME'], sha256json)
            self.hardlink('androidVersion', report['ANDROID_VERSION'], sha256json)
            self.hardlink('product', report['PRODUCT'], sha256json)
            self.hardlink('brand', self.get_brand_from_report(report), sha256json)
            self.hardlink('phoneModel', report['PHONE_MODEL'], sha256json)

            header = make_header(decode_header(message['from']))
            name, address = parseaddr(str(header))
            address = address.lower()
            self.hardlink('address', address, sha256json)

            message_id = self.get_message_id(message)
            self.message_ids.add(message_id)

            # Add things to the report only after its written to disk,
            # this preserves the exact bytes of ACRA-report.stacktrace.json
            # in the file on disk for easy tracking and
            # identification.
            report['Message-ID'] = message_id
            self.hardlink('messageId', report['Message-ID'], sha256json)

            # When there is an attachment, the body is only what the user wrote.
            user_comment = SIGNATURE_BLOCK_PAT.split(self.strip_block(body))[0].strip()
            if user_comment:
                report['USER_COMMENT'] = user_comment

        except Exception as e:
            print('fail', message['from'], str(e), file=sys.stderr)
            traceback.print_exc()

        maildate = message.get('date')
        if maildate:
            # normalize timestamp to per-day in UTC
            report['DATE'] = (
                parsedate_to_datetime(maildate)
                .replace(tzinfo=timezone.utc)
                .strftime('%Y-%m-%d')
            )
            self.hardlink('date', report['DATE'], sha256json)
        else:
            print('Skipping because of bad maildate')
            return

        return report

    def generate_reports_from_mbox(self, mboxfile):
        reports = {}
        print('Processing messages', end='')
        for message in mailbox.mbox(mboxfile):
            print('.', end='', flush=True)
            report = self.process_message(message)
            if not report or 'STACK_TRACE_HASH' not in report:
                continue
            # standardize stack trace hash as fixed 7-char ID
            try:
                stack_trace_hash = report['STACK_TRACE_HASH'][:7].lower()
            except TypeError as e:
                print('TypeError', e, report)
            if stack_trace_hash not in reports:
                reports[stack_trace_hash] = []
            reports[stack_trace_hash].append(report)

        return reports

    def init_gitlab(self):
        private_token = os.getenv('PERSONAL_ACCESS_TOKEN')
        if not private_token:
            print(
                Fore.RED
                + 'ERROR: GitLab Token not found in PERSONAL_ACCESS_TOKEN!'
                + Style.RESET_ALL
            )
            sys.exit(1)
        self.gl = gitlab.Gitlab(
            'https://gitlab.com', api_version=4, private_token=private_token
        )
        self.project = self.gl.projects.get(os.getenv('CI_PROJECT_PATH'), lazy=True)
        warnings.filterwarnings('ignore', category=DeprecationWarning)

    def get_latest_client_release_tag(self):
        project = self.gl.projects.get('fdroid/fdroidclient', lazy=True)
        tags = set()
        for tag in project.tags.list(as_list=False):
            if re.match(r'[1-9]+\.[0-9+].*', tag.name):
                tags.add(tag.name)

        def gen_sort_key(tag):
            # Replace -alpha with a -1 bugfix version
            l = [
                int(t.replace('_', '-'))
                for t in tag.replace('-alpha', '._1.').split('-')[0].split('.')
            ]
            if len(l) < 3:
                l += [0]  # ensure semanic versions
            return l

        return sorted(tags, key=gen_sort_key)[-1]

    def get_existing_from_gitlab(self):
        self.posted_message_ids = set()
        self.existing_labels = set()
        self.existing_issues = dict()
        for issue in self.project.issues.list(state='all', as_list=False):
            labels = set(issue.labels)
            print('issue', issue.get_id())
            for label in labels:
                self.existing_labels.add(label)
                # 0 when no stacktrace https://gitlab.com/fdroid/acra-crash-reports/-/issues/136
                if STACK_TRACE_HASH_ID_PATTERN.match(label) or label == '0':
                    self.existing_issues[label] = issue.get_id()
            m = MESSAGE_ID_PATTERN.search(re.sub(r'\s', r'', issue.description))
            if m:
                self.posted_message_ids.add(m.group())
            for note in issue.notes.list(sort='asc', as_list=False):
                m = MESSAGE_ID_PATTERN.search(re.sub(r'\s', r'', note.body))
                if m:
                    self.posted_message_ids.add(m.group())

        for label in self.project.labels.list(as_list=False):
            self.existing_labels.add(label.name)

    def get_title_from_report(self, report):
        """Return the first normalized line of the stack trace"""
        stack_trace = report.get('STACK_TRACE')
        if not stack_trace:
            return None
        sanitized = re.sub(r'\s+', r' ', stack_trace[:300])
        m = TITLE_FROM_STACK_TRACE_PATTERN.search(sanitized)
        if m:
            title = m.group(1)
        else:
            title = sanitized or report.get('STACK_TRACE_HASH')
        return title.strip()[:255]

    def post_report(self, stack_trace_hash, report_list):
        notes = []
        title = None
        labels = {stack_trace_hash}
        crash_report_count = 0
        for report in sorted(report_list, key=lambda k: k['DATE']):
            crash_report_count += 1
            if 'Message-ID' not in report:
                print('Message-ID not in report:')
                print(yaml.dump(report, allow_unicode=True, default_flow_style=False))
                continue
            if report['Message-ID'] in self.posted_message_ids:
                print('skipping', report['Message-ID'])
                continue
            if not title:
                title = self.get_title_from_report(report)
            for k in ('PACKAGE_NAME', 'APP_VERSION_NAME', 'CountryCode'):
                if report.get(k):
                    labels.add(report[k])
            if report.get('BRAND'):
                labels.add(self.get_brand_from_report(report))
            if report.get('ANDROID_VERSION'):
                labels.add('Android %s' % report['ANDROID_VERSION'])
            if 'USER_COMMENT' in report:
                # gitlab freaks out with too many comments, only post reports with notes
                notes.append(self.generate_note(report))

        if crash_report_count > 1000:
            labels.add('> 1000 reports')
        if crash_report_count > 500:
            labels.add('> 500 reports')
        if crash_report_count > 100:
            labels.add('> 100 reports')
        if crash_report_count > 50:
            labels.add('> 50 reports')
        if crash_report_count > 10:
            labels.add('> 10 reports')

        affects_recent_version = False
        for label in labels:
            if '.' in label:
                try:
                    version = LooseVersion(label.split('-')[0])
                    if version > LooseVersion('1.14') and version < LooseVersion('2.0'):
                        affects_recent_version = True
                        break
                except TypeError:
                    pass
        if not affects_recent_version:
            return

        for label in labels:
            if label in self.existing_labels:
                continue
            try:
                self.project.labels.create({'name': label, 'color': '#eee'})
            except gitlab.exceptions.GitlabCreateError:
                pass

        print('PROCESSING', stack_trace_hash)
        # if stack_trace_hash is in existing issue, don't create a dup, even if closed
        existing_issue_id = self.existing_issues.get(stack_trace_hash)
        if existing_issue_id:
            issue = self.project.issues.get(existing_issue_id)
        else:
            # TODO instead of report_list[0], get first report with current version, else report_list[0]
            issue_dict = {
                    'description': self.generate_note(
                        report_list[0], include_stack_trace=True
                    ),
                    'labels': sorted(labels),
                    'title': title or stack_trace_hash,
                }
            issue = self.project.issues.create(issue_dict)

        # this always counts all reports, so subtract the current tally from the total
        human_total_time_spent = issue.time_stats().get('human_total_time_spent')
        if human_total_time_spent:
            current_time_spent = int(human_total_time_spent.strip('h'))
        else:
            current_time_spent = 0
        spent_time = crash_report_count - current_time_spent
        if spent_time > 0:
            issue.add_spent_time(spent_time)

        print('Creating comments', end='')
        for note in notes:
            print('.', end='', flush=True)
            try:
                issue.notes.create({'body': note[:1000000]})
                time.sleep(0.1)
            except (
                KeyError,
                gitlab.exceptions.GitlabCreateError,
                gitlab.exceptions.GitlabHttpError,
            ) as e:
                print('\nHIT RATE LIMIT', e, sep='\n', flush=True)
                time.sleep(60)
                issue.notes.create({'body': note[:1000000]})
        issue.save()

    def post_reports(self, reports):
        latest_client_release = self.get_latest_client_release_tag()
        print('Using %s as latest release.' % latest_client_release)
        for stack_trace_hash, report_list in reports.items():
            print('length check:', stack_trace_hash, len(report_list), flush=True)
            has_latest_fdroid = False
            for report in report_list:
                avn = report.get('APP_VERSION_NAME')
                if avn and avn.startswith(latest_client_release):
                    has_latest_fdroid = True
                    break
            if has_latest_fdroid or len(report_list) > 25:
                self.post_report(stack_trace_hash, report_list)


def main():
    std = StackTraceDumper()
    reports = std.generate_reports_from_mbox('fdroid-reports')
    with open('reports.yaml', 'w', encoding='utf-8', errors='replace') as fp:
        yaml.safe_dump(reports, fp, allow_unicode=True, default_flow_style=False)

    std.init_gitlab()
    std.get_existing_from_gitlab()
    std.post_reports(reports)


if __name__ == "__main__":
    main()
