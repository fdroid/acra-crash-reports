#!/usr/bin/env python3

import bcrypt
import gitlab
import hashlib
import json
import mailbox
import os
import pycountry
import re
import sys
import time
import traceback
import urllib
import yaml

from colorama import Fore, Style
from datetime import timezone
from distutils.version import LooseVersion
from email.header import decode_header, make_header
from email.utils import parseaddr, parsedate_to_datetime

try:
    from yaml import CSafeLoader as SafeLoader
except ImportError:
    from yaml import SafeLoader


TITLE_FROM_STACK_TRACE_PATTERN = re.compile(r'^(.*?)(?:\s+at\s+|\s+[A-Z0-9_]+=)')

# The stack trace hash standardized to 7 lowercase chars to serve as an ID
STACK_TRACE_HASH_ID_PATTERN = re.compile(r'[0-9a-f]{7}')

# https://en.wikipedia.org/wiki/Bcrypt#Description
MESSAGE_ID_IN_COMMENT_PATTERN = re.compile(r'\$2b\$12\$[A-Za-z0-9/.]{53}')


def strip_block(text):
    if not text or text == 'null':
        return ''
    text = text.replace('<br>', '\n').replace('\r', ' ')
    text = strip_email_addresses_pattern.sub(' ', text).strip()
    if not text or text == 'null':
        return ''
    return text


def get_brand_from_report(report):
    brand = report.get('BRAND')
    if not brand:
        return
    bl = brand.lower()
    if bl in ('lge', 'zte', 'ngm', 'rca', 'kddi', 'semc', 'tcl', 'htc'):
        return bl.upper()
    elif bl == 'htc_europe':
        return 'HTC_Europe'
    elif bl == 'oneplus':
        return 'OnePlus'
    else:
        return brand.lower().title()


def get_title_from_report(report):
    """Return the first normalized line of the stack trace"""
    stack_trace = report.get('STACK_TRACE')
    if not stack_trace:
        return
    sanitized = re.sub(r'\s+', r' ', stack_trace[:300])
    m = TITLE_FROM_STACK_TRACE_PATTERN.search(sanitized)
    if m:
        title = m.group(1)
    else:
        title = sanitized or stack_trace_hash
    return title.strip()[:255]


def generate_note(report, include_stack_trace=False):
    if 'STACK_TRACE' in report:
        stack_trace = report.get('STACK_TRACE')
    else:
        stack_trace = ''
    if 'STACK_TRACE' in report:
        d = report.copy()
        d.pop('STACK_TRACE')
        report = d
    # sanitize JSON report
    for k in ('DISPLAY', 'IS_SILENT', 'REPORT_ID', 'USER_CRASH_DATE', 'USER_EMAIL'):
        if k in report:
            del report[k]
    description = yaml.safe_dump(report, allow_unicode=True, default_flow_style=False)
    if include_stack_trace and stack_trace:
        # manually dump for cleaner format
        description += '\n```\n## STACK_TRACE:\n\n```java\n'
        description += stack_trace.replace('\nat ', '\n  at ')
    return '```yaml\n%s\n```\n\n%s\n' % (
        description,
        '@' + ' @'.join(report.get('ping', [])),
    )

def get_message_id(message):
    """Get a anonymity-preserving message ID

    This is meant to be a one way Message ID, so that someone can find
    the right message if they have the email mbox.  This can be used
    to find the original email if you have the entire text of it.

    All emails should have a Message-ID, but sometimes they don't.  So
    the failsafe is to hash the whole email.

    """

    message_id = message.get('Message-ID')
    if message_id:
        to_sha256 = str(make_header(decode_header(message_id))).encode()
    else:
        to_sha256 = message.as_bytes()
    to_bcrypt = hashlib.sha256(to_sha256).hexdigest().encode()
    salt = b'$2b$12$%s' % hashlib.sha256(to_bcrypt).hexdigest().encode()[:22]
    return bcrypt.hashpw(to_bcrypt, salt).decode()


names = set()
addresses = set()
domains = dict()

tld_countries = {
    'berlin': pycountry.countries.get(alpha_2='DE'),
    'cat': pycountry.countries.get(alpha_2='ES'),
    'edu': pycountry.countries.get(alpha_2='US'),
    'hamburg': pycountry.countries.get(alpha_2='DE'),
    'ovh': pycountry.countries.get(alpha_2='FR'),
    'scot': pycountry.countries.get(alpha_2='GB'),
    'tokyo': pycountry.countries.get(alpha_2='JP'),
    'uk': pycountry.countries.get(alpha_2='GB'),
}

# map of email address to gitlab account
core_contributors = {'hans@guardianproject.info': 'eighthave'}

app_version_name_pattern = re.compile(r'APP_VERSION_NAME=([^\sA-Z=<]+)')
android_version_pattern = re.compile(r'ANDROID_VERSION=([^\sA-Z=<]+)')
brand_pattern = re.compile(r'BRAND=([\w -]{3,}?) *(?:[A-Z]{3,}[A-Z_]+[A-Z]=|\n)')
phone_model_pattern = re.compile(
    r'PHONE_MODEL=([\w -]{3,}?) *(?:[A-Z]{3,}[A-Z_]+[A-Z]=|\n)'
)
available_mem_size_pattern = re.compile(r'AVAILABLE_MEM_SIZE=([0-9]+)')
# 7 chars is enough to track it, and some clients only submit 7
# hash algorithm: https://github.com/ACRA/acra/pull/89/files
stack_trace_hash_pattern = re.compile(r'STACK_TRACE_HASH=([a-f0-9]{7,})')
user_comment_pattern = re.compile(
    r'USER_COMMENT=([^=]*)(?:\n\s*[A-Z0-9_]+=|\n\n)', re.DOTALL
)
custom_data_pattern = re.compile(
    r'CUSTOM_DATA=([^=]*)(?:\n\s*[A-Z0-9_]+=|\n\n)', re.DOTALL
)
stack_trace_pattern = re.compile(
    r'STACK_TRACE=([^=]*)(?:\n\s*[A-Z0-9_]+=|\n\n)', re.DOTALL
)

strip_email_addresses_pattern = re.compile(r'\S+@\S+\.\S+')
whitespace_pattern = re.compile(r'\s')


def process_message(message):
    report = {}
    body = ''
    try:
        found_attachment = False
        if message.is_multipart():
            for part in message.walk():
                if part.get_content_type() in ('message/rfc822', 'text/plain'):
                    body += part.get_payload(decode=True).decode(errors='replace')
                elif part.get_filename() == 'ACRA-report.stacktrace':
                    report = json.loads(part.get_payload(decode=True))
                    found_attachment = True
        else:
            body = message.get_payload(decode=True).decode(errors='replace')

        body = strip_block(body)
        if found_attachment and whitespace_pattern.sub('', body):
            # When there is an attachment, the body is only what the user wrote.
            report['USER_COMMENT'] = body.strip()

        report['Message-ID'] = get_message_id(message)
        # print('.', end='', flush=True)

        subject = str(make_header(decode_header(message.get('Subject', ''))))
        if not report.get('PACKAGE_NAME') and ':' in subject:
            m = re.search(r'.*?([a-z][a-zA-Z0-9._-]+):.*', subject)
            if m:
                report['PACKAGE_NAME'] = m.group(1)

        header = make_header(decode_header(message['from']))
        name, address = parseaddr(str(header))
        address = address.lower()
        names.add(name)
        addresses.add(address)
        ping = core_contributors.get(address)
        if ping:
            if 'ping' not in report:
                report['ping'] = []
            report['ping'].append(ping)

        domain = address.split('@')[1].lower()
        if domain in domains:
            domains[domain] += 1
        else:
            domains[domain] = 1

        tld = domain.split('.')[-1]
        country = None
        countrycode = ''
        try:
            if len(tld) == 2:
                country = pycountry.countries.get(alpha_2=tld.upper())
            elif len(tld) == 3 and tld != 'com':
                country = pycountry.countries.get(alpha_3=tld.upper())
            else:
                country = pycountry.countries.get(alpha_4=tld.upper())
        except KeyError as e:
            country = tld_countries.get(tld)
        if country:
            report['CountryCode'] = country.alpha_2

    except Exception as e:
        print('fail', message['from'], str(e), file=sys.stderr)
        traceback.print_exc()
        if not body:
            return

    maildate = message.get('date')
    if maildate:
        # normalize timestamp to per-day in UTC
        report['DATE'] = (
            parsedate_to_datetime(maildate)
            .replace(tzinfo=timezone.utc)
            .strftime('%Y-%m-%d')
        )
    else:
        print('Skipping because of bad maildate')
        return

    if not found_attachment:
        # no JSON attachment, parse body
        process_body(body, report)

    return report


def process_body(body, report):
    m = app_version_name_pattern.search(body)
    if m:
        app_version_name = m.group(1).strip()
        if app_version_name and app_version_name != 'null':
            report['APP_VERSION_NAME'] = app_version_name

    m = android_version_pattern.search(body)
    if m:
        android_version = m.group(1).strip()
        if android_version and android_version != 'null':
            report['ANDROID_VERSION'] = android_version

    m = brand_pattern.search(body)
    if m:
        brand = m.group(1).strip()
        if brand and brand != 'null':
            report['BRAND'] = brand

    m = phone_model_pattern.search(body)
    if m:
        phone_model = m.group(1).strip()
        if phone_model and phone_model != 'null':
            report['PHONE_MODEL'] = phone_model

    m = available_mem_size_pattern.search(body)
    if m:
        try:
            report['AVAILABLE_MEM_SIZE'] = int(m.group(1).strip())
        except ValueError:
            pass

    m = custom_data_pattern.search(body)
    if m:
        custom_data = strip_email_addresses_pattern.sub(
            ' ', m.group(1).strip().replace('\r', '')
        )
        if custom_data and custom_data != 'null':
            report['CUSTOM_DATA'] = custom_data

    m = user_comment_pattern.search(body)
    if m:
        user_comment = strip_email_addresses_pattern.sub(
            ' ', m.group(1).strip().replace('\r', '')
        )
        if user_comment and user_comment != 'null':
            report['USER_COMMENT'] = user_comment

    m = stack_trace_pattern.search(body)
    if m:
        stack_trace = strip_email_addresses_pattern.sub(
            ' ', m.group(1).strip().replace('\r', '')
        )
        if stack_trace and stack_trace != 'null':
            report['STACK_TRACE'] = stack_trace

    m = stack_trace_hash_pattern.search(body)
    if m:
        report['STACK_TRACE_HASH'] = m.group(1).strip()

    return report


def generate_reports_from_mbox(mboxfile):
    reports = {}
    print('Processing messages', end='')
    for message in mailbox.mbox(mboxfile):
        print('.', end='', flush=True)
        report = process_message(message)
        if not report or 'STACK_TRACE_HASH' not in report:
            continue
        stack_trace_hash = report['STACK_TRACE_HASH'][:7].lower()  # standardized as ID
        if stack_trace_hash not in reports:
            reports[stack_trace_hash] = []
        reports[stack_trace_hash].append(report)
    return reports


def get_latest_client_release_tag(gl):
    project = gl.projects.get('fdroid/fdroidclient', lazy=True)
    tags = set()
    for tag in project.tags.list(as_list=False):
        if re.match(r'[1-9]+\.[0-9+].*', tag.name):
            tags.add(tag.name)
    return sorted(
        tags,
        key=lambda x: [
            int(t) for t in x.replace('-alpha', '.0.').split('-')[0].split('.')
        ],
    )[-1]


if os.path.exists('reports.yaml'):
    print('Loading existing reports.yaml')
    with open('reports.yaml', encoding='utf-8', errors='replace') as fp:
        reports = yaml.load(fp, Loader=SafeLoader)
else:
    reports = generate_reports_from_mbox('fdroid-reports')
    with open('reports.yaml', 'w', encoding='utf-8', errors='replace') as fp:
        yaml.safe_dump(reports, fp, allow_unicode=True, default_flow_style=False)


private_token = os.getenv('PERSONAL_ACCESS_TOKEN')
if not private_token:
    print(
        Fore.RED
        + 'ERROR: GitLab Token not found in PERSONAL_ACCESS_TOKEN!'
        + Style.RESET_ALL
    )
    sys.exit(1)
gl = gitlab.Gitlab('https://gitlab.com', api_version=4, private_token=private_token)

latest_client_release = get_latest_client_release_tag(gl)

project = gl.projects.get(os.getenv('CI_PROJECT_PATH'), lazy=True)
posted_message_ids = set()
existing_labels = set()
existing_issues = dict()
for issue in project.issues.list(state='all', order_by='updated_at', as_list=False):
    labels = set(issue.labels)
    for label in labels:
        existing_labels.add(label)
        # 0 when no stacktrace https://gitlab.com/fdroid/acra-crash-reports/-/issues/136
        if STACK_TRACE_HASH_ID_PATTERN.match(label) or label == '0':
            existing_issues[label] = issue.get_id()
    m = MESSAGE_ID_IN_COMMENT_PATTERN.search(re.sub(r'\s', r'', issue.description))
    if m:
        posted_message_ids.add(m.group())
    for note in issue.notes.list(sort='asc', as_list=False):
        m = MESSAGE_ID_IN_COMMENT_PATTERN.search(re.sub(r'\s', r'', note.body))
        if m:
            posted_message_ids.add(m.group())

for label in project.labels.list(as_list=False):
    existing_labels.add(label.name)

for stack_trace_hash, report_list in reports.items():
    has_latest_fdroid = False
    for report in report_list:
        if report.get('APP_VERSION_NAME', '').startswith(latest_client_release):
            has_latest_fdroid = True
            break
    if has_latest_fdroid or len(report_list) > 25:
        notes = []
        title = None
        labels = {stack_trace_hash}
        crash_report_count = 0
        for report in sorted(report_list, key=lambda k: k['DATE']):
            crash_report_count += 1
            if report['Message-ID'] in posted_message_ids:
                print('skipping', report['Message-ID'])
                continue
            if not title:
                title = get_title_from_report(report)
            for k in ('PACKAGE_NAME', 'APP_VERSION_NAME', 'CountryCode'):
                if report.get(k):
                    labels.add(report[k])
            if report.get('BRAND'):
                labels.add(get_brand_from_report(report))
            if report.get('ANDROID_VERSION'):
                labels.add('Android %s' % report['ANDROID_VERSION'])
            if 'USER_COMMENT' in report:
                # gitlab freaks out with too many comments, only post reports with notes
                notes.append(generate_note(report))

        if crash_report_count > 1000:
            labels.add('> 1000 reports')
        elif crash_report_count > 500:
            labels.add('> 500 reports')
        elif crash_report_count > 100:
            labels.add('> 100 reports')
        elif crash_report_count > 50:
            labels.add('> 50 reports')
        elif crash_report_count > 10:
            labels.add('> 10 reports')

        affects_recent_version = False
        for label in labels:
            if '.' in label:
                try:
                    version = LooseVersion(label.split('-')[0])
                    if version > LooseVersion('1.14') and version < LooseVersion('2.0'):
                        affects_recent_version = True
                        break
                except TypeError:
                    pass
        if not affects_recent_version:
            continue

        for label in labels:
            if label in existing_labels:
                continue
            try:
                project.labels.create({'name': label, 'color': '#eee'})
            except gitlab.exceptions.GitlabCreateError as e:
                pass

        # if stack_trace_hash is in existing issue, don't create a dup, even if closed
        existing_issue_id = existing_issues.get(stack_trace_hash)
        if existing_issue_id:
            issue = project.issues.get(existing_issue_id)
        else:
            # TODO instead of report_list[0], get first report with current version, else report_list[0]
            issue = project.issues.create(
                {
                    'description': generate_note(report_list[0], include_stack_trace=True),
                    'labels': sorted(labels),
                    'title': title or stack_trace_hash,
                }
            )

        # this always counts all reports, so subtract the current tally from the total
        human_total_time_spent = issue.time_stats().get('human_total_time_spent')
        if human_total_time_spent:
            current_time_spent = int(human_total_time_spent.strip('h'))
        else:
            current_time_spent = 0
        spent_time = crash_report_count - current_time_spent
        if spent_time > 0:
            issue.add_spent_time(spent_time)

        print('Creating comments', end='')
        for note in notes:
            print('.', end='', flush=True)
            try:
                issue.notes.create({'body': note[:1000000]})
                time.sleep(0.1)
            except (
                KeyError,
                gitlab.exceptions.GitlabCreateError,
                gitlab.exceptions.GitlabHttpError,
            ) as e:
                print('\nHIT RATE LIMIT', e, sep='\n', flush=True)
                time.sleep(60)
                issue.notes.create({'body': note[:1000000]})
        issue.save()
    else:
        for r in report_list:
            if 'USER_COMMENT' in r:
                pass
# TODO                 issue.state_event = 'close'
