#!/usr/bin/env python3

import os
import gitlab
import sys
from colorama import Fore, Style
from strictyaml import load, Map, Str, Seq

COLORS = {
    # bright
    'actions': '#0bdf08',  # green
    'warnings': '#ffc040',  # orange
    'errors': '#ff0000',  # red
    # smooth
    'features': '#eed5ee',  # light pink
    'languages': '#e8d8cb',  # light brown
    'tools': '#bfe7f2',  # light blue
    'status': '#dbecd8',  # light green
    'group_funding': '#edf2d5',  # light yellow
    'manual_status': '#0000ff',  # blue
}


def main():
    private_token = os.getenv('PERSONAL_ACCESS_TOKEN')
    if not private_token:
        print(
            Fore.RED
            + 'ERROR: GitLab Token not found in PERSONAL_ACCESS_TOKEN!'
            + Style.RESET_ALL
        )
        sys.exit(1)
    path = os.getenv('CI_PROJECT_PATH')
    if not path:
        print(
            Fore.RED
            + 'ERROR: project path not found CI_PROJECT_PATH!'
            + Style.RESET_ALL
        )
        sys.exit(1)
    gl = gitlab.Gitlab('https://gitlab.com', api_version=4, private_token=private_token)
    project = gl.projects.get(path, lazy=True)

    schema = Map(
        {
            'status': Seq(Str()),
            'actions': Seq(Str()),
            'errors': Seq(Str()),
            'warnings': Seq(Str()),
            'tools': Seq(Str()),
            'features': Seq(Str()),
            'languages': Seq(Str()),
            'group_funding': Seq(Str()),
            'manual_status': Seq(Str()),
        }
    )
    with open('label-categories.yaml') as fp:
        mapping = load(fp.read(), schema).data

    labels_to_modify = set()
    for labels in mapping.values():
        for l in labels:
            labels_to_modify.add(str(l))
    print('labels to modify:', sorted(labels_to_modify))
    print('label categories:', sorted(mapping.keys()))

    skipped = set()
    for page in range(20):  # this has to be big enough to get all labels
        for label in project.labels.list(page=page, per_page=100):
            if not label.is_project_label:
                continue
            if '.' not in label.name and label.name not in labels_to_modify:
                skipped.add(label.name)
                continue
            label.color = '#eee'
            for k, v in mapping.items():
                if label.name in v:
                    label.color = COLORS[k]
            print(label.name, label.color)
            label.save()
    print('skipped:', sorted(skipped))


if __name__ == "__main__":
    main()
