#!/usr/bin/env python3

import mailbox
import pycountry
import re
import sys
import time
import urllib

from alphabet_detector import AlphabetDetector
from email.header import decode_header, make_header
from email.utils import parseaddr, parsedate_to_datetime

ad = AlphabetDetector()

names = set()
addresses = set()
domains = dict()
tlds = set()
alphabets = set()

tld_countries = {
    'berlin': pycountry.countries.get(alpha_2='DE'),
    'cat': pycountry.countries.get(alpha_2='ES'),
    'edu': pycountry.countries.get(alpha_2='US'),
    'hamburg': pycountry.countries.get(alpha_2='DE'),
    'ovh': pycountry.countries.get(alpha_2='FR'),
    'scot': pycountry.countries.get(alpha_2='GB'),
    'tokyo': pycountry.countries.get(alpha_2='JP'),
    'uk': pycountry.countries.get(alpha_2='GB'),
}

alphabet_countries = {
    'ARABIC': pycountry.countries.get(alpha_2='EG'),
    'CJK': pycountry.countries.get(alpha_2='CN'),
    'CYRILLIC': pycountry.countries.get(alpha_2='RU'),
    'ETHIOPIC': pycountry.countries.get(alpha_2='ET'),
    'GEORGIAN': pycountry.countries.get(alpha_2='GE'),
    'GREEK': pycountry.countries.get(alpha_2='GR'),
    'HANGUL': pycountry.countries.get(alpha_2='KR'),
    'HEBREW': pycountry.countries.get(alpha_2='IL'),
    'HIRAGANA': pycountry.countries.get(alpha_2='JP'),
    'KATAKANA': pycountry.countries.get(alpha_2='JP'),
    'THAI': pycountry.countries.get(alpha_2='TH'),
}

fdroid_pattern = re.compile(r'APP_VERSION_NAME=(\S+)')
android_pattern = re.compile(r'ANDROID_VERSION=(\S+)')
brand_pattern = re.compile(r'BRAND=([\w ]+)')
phone_pattern = re.compile(r'PHONE_MODEL=([\w ]+)')
subject_pattern = re.compile(r'STACK_TRACE=(.*)')

mbox = mailbox.mbox('ACRA')
for message in mbox:
    try:
        header = make_header(decode_header(message['from']))
        name, address = parseaddr(str(header))
        names.add(name)
        addresses.add(address)

        domain = address.split('@')[1].lower()
        if domain in domains:
            domains[domain] += 1
        else:
            domains[domain] = 1

        tld = domain.split('.')[-1]
        country = None
        countrycode = ''
        try:
            if len(tld) == 2:
                country = pycountry.countries.get(alpha_2=tld.upper())
            elif len(tld) == 3 and tld != 'com':
                country = pycountry.countries.get(alpha_3=tld.upper())
            else:
                country = pycountry.countries.get(alpha_4=tld.upper())
        except KeyError:
            country = tld_countries.get(tld)
        if country:
            countrycode = country.alpha_2

        if tld not in ('aero', 'biz', 'com', 'eu', 'org', 'info', 'net', 'name', 'ninja',
                       'one', 'party', 'xyz', 'xxx'):
            tlds.add(tld)
            #if country is None:
            #    print('not a country:', tld)

        alphabet = ad.detect_alphabet(name)
        for a in alphabet:
            alphabets.add(a)
        if name and alphabet and alphabet != {'LATIN'}:
            if 'LATIN' in alphabet:
                alphabet.remove('LATIN')
            #print('alphabet', name, alphabet)
            if alphabet and country is None:
                country = alphabet_countries.get(alphabet.pop())

        if message.is_multipart():
            body = ''
            for part in message.walk():
                if part.get_content_type() in ('message/rfc822', 'text/plain'):
                    body += part.get_payload(decode=True).decode()
        else:
            body = message.get_payload(decode=True).decode()
        print('\n\nBODY:', body, '\n\n')
    except Exception as e:
        print('fail', message['from'], str(e), file=sys.stderr)

    date = parsedate_to_datetime(message['date']).utctimetuple()

    m = subject_pattern.search(body)
    filename = ''
    if m:
        filename = urllib.parse.quote(m.group(1).strip())
        print('filename', filename)

    m = fdroid_pattern.search(body)
    fdroid = ''
    if m:
        fdroid = m.group(1).strip()

    m = android_pattern.search(body)
    android = ''
    if m:
        android = m.group(1).strip()

    m = brand_pattern.search(body)
    brand = ''
    if m:
        brand = m.group(1).strip()

    m = phone_pattern.search(body)
    phone = ''
    if m:
        phone = m.group(1).strip()

    useragent = ('Mozilla/5.0 (Linux; Android {android}; {brand} {phone}) F-Droid/{fdroid}'
                 .format(fdroid=fdroid, brand=brand, phone=phone, android=android))

    print('0.0.0.0 - - [{date}] "GET {filename} HTTP/1.1" 200 147950 "-" "{useragent}" {countrycode}'
          .format(date=time.strftime('%d/%b/%Y:%H:%M:%S %z', date),
                  filename=filename,
                  useragent=useragent,
                  countrycode=countrycode))


import pprint

public_domains = dict()
for k, v in domains.items():
    if v > 50:
        public_domains[k] = v
pprint.pprint(sorted(public_domains.items()))

#pprint.pprint(sorted(tlds))
#pprint.pprint(sorted(alphabets))


print(len(mbox), len(names), len(addresses))


